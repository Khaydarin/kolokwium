package kolok11stycznia;
import java.awt.*;
import javax.swing.*;


public class Panel extends JPanel {

	private static final long serialVersionUID = 1L;

	public Panel(){
		setPreferredSize(new Dimension(400, 400));
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		g2d.drawOval(10, 10, 380, 380);
	}
}
	
